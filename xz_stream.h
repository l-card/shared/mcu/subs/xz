#ifndef XZ_STRAM_H
#define XZ_STRAM_H

#include "xz.h"
#include "xz_lzma2.h"

typedef uint64_t vli_type;

/* Maximum encoded size of a VLI */
#define VLI_BYTES_MAX (sizeof(vli_type) * 8 / 7)
#define VLI_UNKNOWN ((vli_type)-1)

#define STREAM_HEADER_SIZE 12

#define XZ_HEADER_MAGIC "\3757zXZ"
#define XZ_HEADER_MAGIC_SIZE 6

#define XZ_FOOTER_MAGIC "YZ"
#define XZ_FOOTER_MAGIC_SIZE 2





/* Hash used to validate the Index field */
typedef struct  {
    vli_type unpadded;
    vli_type uncompressed;
    uint32_t crc32;
} xz_dec_hash_t;

typedef struct  {
    /* Position in dec_main() */
    enum {
        SEQ_STREAM_HEADER,
        SEQ_BLOCK_START,
        SEQ_BLOCK_HEADER,
        SEQ_BLOCK_UNCOMPRESS,
        SEQ_BLOCK_PADDING,
        SEQ_BLOCK_CHECK,
        SEQ_INDEX,
        SEQ_INDEX_PADDING,
        SEQ_INDEX_CRC32,
        SEQ_STREAM_FOOTER
    } sequence;

    /* Position in variable-length integers and Check fields */
    uint32_t pos;
    /* Variable-length integer decoded by dec_vli() */
    vli_type vli;

    /* Saved in_pos and out_pos */
    size_t in_start;
    size_t out_start;

    /* CRC32 value in Block or Index */
    uint32_t crc32;

	/* Type of the integrity check calculated from uncompressed data */
	uint8_t check_type;

    /* Information stored in Block Header */
    struct {
        /*
         * Value stored in the Compressed Size field, or
         * VLI_UNKNOWN if Compressed Size is not present.
         */
        vli_type compressed;

        /*
         * Value stored in the Uncompressed Size field, or
         * VLI_UNKNOWN if Uncompressed Size is not present.
         */
        vli_type uncompressed;

        /* Size of the Block Header field */
        uint32_t size;
    } block_header;

    /* Information collected when decoding Blocks */
    struct {
        /* Observed compressed size of the current Block */
        vli_type compressed;

        /* Observed uncompressed size of the current Block */
        vli_type uncompressed;

        /* Number of Blocks decoded so far */
        vli_type count;

        /*
         * Hash calculated from the Block sizes. This is used to
         * validate the Index field.
         */
        xz_dec_hash_t hash;
    } block;

    /* Variables needed when verifying the Index field */
    struct {
        /* Position in dec_index() */
        enum {
            SEQ_INDEX_COUNT,
            SEQ_INDEX_UNPADDED,
            SEQ_INDEX_UNCOMPRESSED
        } sequence;

        /* Size of the Index in bytes */
        vli_type size;

        /* Number of Records (matches block.count in valid files) */
        vli_type count;

        /*
         * Hash calculated from the Records (matches block.hash in
         * valid files).
         */
         xz_dec_hash_t hash;
    } index;


    /*
     * Temporary buffer needed to hold Stream Header, Block Header,
     * and Stream Footer. The Block Header is the biggest (1 KiB)
     * so we reserve space according to that. buf[] has to be aligned
     * to a multiple of four bytes; the size_t variables before it
     * should guarantee this.
     */
    struct {
        size_t pos;
        size_t size;
        uint8_t buf[1024];
    } temp;


    t_xz_dec_lzma2 lzma2;

} t_xz_dec;

void xz_dec_reset(t_xz_dec *s);
int xz_dec_run(t_xz_dec *s, t_xz_buf *b);


#endif // XZ_STRAM_H
