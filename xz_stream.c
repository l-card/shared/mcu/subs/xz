#include "xz_stream.h"
#include "xz_crc32.h"
#include "string.h"

static uint32_t f_get_le32(uint8_t* buf)
{
    return (buf[0] | ((uint32_t)buf[1] << 8)
            | ((uint32_t)buf[2] << 16) | ((uint32_t)buf[3] << 24));
}




/* Decode a variable-length integer (little-endian base-128 encoding) */
static int f_xz_dec_vli(t_xz_dec *s,
        const uint8_t *in, size_t *in_pos, size_t in_size)
{
    uint8_t byte;

    if (s->pos == 0)
        s->vli = 0;

    while (*in_pos < in_size)
    {
        byte = in[*in_pos];
        ++*in_pos;

        s->vli |= (vli_type)(byte & 0x7F) << s->pos;

        if ((byte & 0x80) == 0)
        {
            /* Don't allow non-minimal encodings. */
            if (byte == 0 && s->pos != 0)
                return XZ_ERR_DATA;

            s->pos = 0;
            return XZ_STREAM_END;
        }
        s->pos += 7;
        if (s->pos == 7 * VLI_BYTES_MAX)
            return XZ_ERR_DATA;
    }

    return XZ_OK;
}





static int f_xz_dec_stream_header(t_xz_dec *s)
{
    if (memcmp(s->temp.buf, XZ_HEADER_MAGIC, XZ_HEADER_MAGIC_SIZE)!=0)
        return XZ_ERR_FORMAT;

    if (xz_crc32(s->temp.buf + XZ_HEADER_MAGIC_SIZE, 2, 0)
            != f_get_le32(s->temp.buf + XZ_HEADER_MAGIC_SIZE + 2))
        return XZ_ERR_CRC;

    if (s->temp.buf[XZ_HEADER_MAGIC_SIZE] != 0)
        return XZ_ERR_OPTIONS;

    /*
     * Of integrity checks, we support only none (Check ID = 0) and
     * CRC32 (Check ID = 1). However, if XZ_DEC_ANY_CHECK is defined,
     * we will accept other check types too, but then the check won't
     * be verified and a warning (XZ_UNSUPPORTED_CHECK) will be given.
     */
    s->check_type = s->temp.buf[XZ_HEADER_MAGIC_SIZE + 1];

#ifdef XZ_DEC_ANY_CHECK
    if (s->check_type > XZ_CHECK_MAX)
        return XZ_ERR_OPTIONS;

    if (s->check_type > XZ_CHECK_CRC32)
        return XZ_ERR_UNSUPPORTED_CHECK;
#else
    if (s->check_type > XZ_CHECK_CRC32)
        return XZ_ERR_OPTIONS;
#endif
    return XZ_OK;
}


/* Decode the Stream Footer field (the last 12 bytes of the .xz Stream) */
static int f_xz_dec_stream_footer(t_xz_dec *s)
{
    if (memcmp(s->temp.buf + 10, XZ_FOOTER_MAGIC, XZ_FOOTER_MAGIC_SIZE)!=0)
        return XZ_ERR_FORMAT;

    if (xz_crc32(s->temp.buf + 4, 6, 0) != f_get_le32(s->temp.buf))
        return XZ_ERR_DATA;

    /*
     * Validate Backward Size. Note that we never added the size of the
     * Index CRC32 field to s->index.size, thus we use s->index.size / 4
     * instead of s->index.size / 4 - 1.
     */
    if ((s->index.size >> 2) != f_get_le32(s->temp.buf + 4))
        return XZ_ERR_DATA;

    //��������� �����, ��� ��� ����� �� ��� � � stream header
    if ((s->temp.buf[8] != 0) || (s->temp.buf[9] != s->check_type))
        return XZ_ERR_DATA;

    /*
     * Use XZ_STREAM_END instead of XZ_OK to be more convenient
     * for the caller.
     */
    return XZ_STREAM_END;
}




/* Decode the Block Header and initialize the filter chain. */
static int f_xz_dec_block_header(t_xz_dec *s)
{
    int ret;

    /*
     * Validate the CRC32. We know that the temp buffer is at least
     * eight bytes so this is safe.
     */
    s->temp.size -= 4;
    if (xz_crc32(s->temp.buf, s->temp.size, 0)
                    != f_get_le32(s->temp.buf + s->temp.size))
        return XZ_ERR_CRC;

    s->temp.pos = 2;

    /*
     * Catch unsupported Block Flags. We support only one or two filters
     * in the chain, so we catch that with the same test.
     */
#ifdef XZ_DEC_BCJ
    if (s->temp.buf[1] & 0x3E)
#else
    if (s->temp.buf[1] & 0x3F)
#endif
        return XZ_ERR_OPTIONS;

    /* Compressed Size */
    if (s->temp.buf[1] & 0x40)
    {
        if (f_xz_dec_vli(s, s->temp.buf, &s->temp.pos, s->temp.size)
                                    != XZ_STREAM_END)
            return XZ_ERR_DATA;

        s->block_header.compressed = s->vli;
    }
    else
    {
        s->block_header.compressed = VLI_UNKNOWN;
    }

    /* Uncompressed Size */
    if (s->temp.buf[1] & 0x80) {
            if (f_xz_dec_vli(s, s->temp.buf, &s->temp.pos, s->temp.size)
                            != XZ_STREAM_END)
                    return XZ_ERR_DATA;

            s->block_header.uncompressed = s->vli;
    } else {
            s->block_header.uncompressed = VLI_UNKNOWN;
    }

#ifdef XZ_DEC_BCJ
    /* If there are two filters, the first one must be a BCJ filter. */
    s->bcj_active = s->temp.buf[1] & 0x01;
    if (s->bcj_active) {
            if (s->temp.size - s->temp.pos < 2)
                    return XZ_OPTIONS_ERROR;

            ret = xz_dec_bcj_reset(s->bcj, s->temp.buf[s->temp.pos++]);
            if (ret != XZ_OK)
                    return ret;

            /*
             * We don't support custom start offset,
             * so Size of Properties must be zero.
             */
            if (s->temp.buf[s->temp.pos++] != 0x00)
                    return XZ_OPTIONS_ERROR;
    }
#endif

    /* Valid Filter Flags always take at least two bytes. */
    if ((s->temp.size - s->temp.pos) < 2)
            return XZ_ERR_DATA;

    /* Filter ID = LZMA2 */
    if (s->temp.buf[s->temp.pos++] != 0x21)
            return XZ_ERR_OPTIONS;

    /* Size of Properties = 1-byte Filter Properties */
    if (s->temp.buf[s->temp.pos++] != 0x01)
            return XZ_ERR_OPTIONS;

    /* Filter Properties contains LZMA2 dictionary size. */
    if ((s->temp.size - s->temp.pos) < 1)
            return XZ_ERR_DATA;

    ret = xz_dec_lzma2_reset(&s->lzma2, s->temp.buf[s->temp.pos++]);

    if (ret != XZ_OK)
        return ret;

    /* The rest must be Header Padding. */
    while (s->temp.pos < s->temp.size)
    {
        if (s->temp.buf[s->temp.pos++] != 0x00)
            return XZ_ERR_OPTIONS;
    }

    s->temp.pos = 0;
    s->block.compressed = 0;
    s->block.uncompressed = 0;

    return XZ_OK;
}


/*
 * Decode the Compressed Data field from a Block. Update and validate
 * the observed compressed and uncompressed sizes of the Block so that
 * they don't exceed the values possibly stored in the Block Header
 * (validation assumes that no integer overflow occurs, since vli_type
 * is normally uint64_t). Update the CRC32 if presence of the CRC32
 * field was indicated in Stream Header.
 *
 * Once the decoding is finished, validate that the observed sizes match
 * the sizes possibly stored in the Block Header. Update the hash and
 * Block count, which are later used to validate the Index field.
 */
static int f_xz_dec_block(t_xz_dec *s, t_xz_buf *b)
{
    int ret;

    s->in_start = b->in_pos;
    s->out_start = b->out_pos;

#ifdef XZ_DEC_BCJ
    if (s->bcj_active)
        ret = xz_dec_bcj_run(s->bcj, s->lzma2, b);
    else
#endif
        ret = xz_dec_lzma2_run(&s->lzma2, b);

    s->block.compressed += b->in_pos - s->in_start;
    s->block.uncompressed += b->out_pos - s->out_start;

    /*
     * There is no need to separately check for VLI_UNKNOWN, since
     * the observed sizes are always smaller than VLI_UNKNOWN.
     */
    if (s->block.compressed > s->block_header.compressed
            || s->block.uncompressed
                > s->block_header.uncompressed)
        return XZ_ERR_DATA;

    if (s->check_type == XZ_CHECK_CRC32)
        s->crc32 = xz_crc32(b->out + s->out_start,
                b->out_pos - s->out_start, s->crc32);

    if (ret == XZ_STREAM_END)
    {
        //���� � ��������� ���� ������� �������
        //�� ������� ��
        if (s->block_header.compressed != VLI_UNKNOWN
                && s->block_header.compressed
                    != s->block.compressed)
            return XZ_ERR_DATA;

        if (s->block_header.uncompressed != VLI_UNKNOWN
                && s->block_header.uncompressed
                    != s->block.uncompressed)
            return XZ_ERR_DATA;

        s->block.hash.unpadded += s->block_header.size
                + s->block.compressed;

#ifdef XZ_DEC_ANY_CHECK
        s->block.hash.unpadded += check_sizes[s->check_type];
#else
        if (s->check_type == XZ_CHECK_CRC32)
            s->block.hash.unpadded += 4;
#endif

        s->block.hash.uncompressed += s->block.uncompressed;
        s->block.hash.crc32 = xz_crc32(
                (const uint8_t *)&s->block.hash,
                sizeof(s->block.hash), s->block.hash.crc32);

        ++s->block.count;
    }

    return ret;
}








/*
 * Fill s->temp by copying data starting from b->in[b->in_pos]. Caller
 * must have set s->temp.pos to indicate how much data we are supposed
 * to copy into s->temp.buf. Return true once s->temp.pos has reached
 * s->temp.size.
 */
static int f_xz_fill_temp(t_xz_dec *s, t_xz_buf *b)
{
    size_t copy_size = MIN(b->in_size - b->in_pos, s->temp.size - s->temp.pos);

    memcpy(s->temp.buf + s->temp.pos, b->in + b->in_pos, copy_size);
    b->in_pos += copy_size;
    s->temp.pos += copy_size;

    if (s->temp.pos == s->temp.size)
    {
        s->temp.pos = 0;
        return TRUE;
    }
    return FALSE;
}

/*
 * Validate that the next four input bytes match the value of s->crc32.
 * s->pos must be zero when starting to validate the first byte.
 */
static int f_crc32_validate(t_xz_dec *s, t_xz_buf *b)
{
    do
    {
        if (b->in_pos == b->in_size)
            return XZ_OK;

        if (((s->crc32 >> s->pos) & 0xFF) != b->in[b->in_pos++])
            return XZ_ERR_DATA;
        s->pos += 8;
    } while (s->pos < 32);

    s->crc32 = 0;
    s->pos = 0;

    return XZ_STREAM_END;
}

#ifdef XZ_DEC_ANY_CHECK
/*
 * Skip over the Check field when the Check ID is not supported.
 * Returns true once the whole Check field has been skipped over.
 */
static int f_check_skip(t_xz_dec *s, t_xz_buf *b)
{
    while (s->pos < check_sizes[s->check_type])
    {
        if (b->in_pos == b->in_size)
            return FALSE;

        ++b->in_pos;
        ++s->pos;
    }

    s->pos = 0;

    return TRUE;
}
#endif


/* Update the Index size and the CRC32 value. */
static void f_xz_index_update(t_xz_dec *s, const t_xz_buf *b)
{
    size_t in_used = b->in_pos - s->in_start;
    s->index.size += in_used;
    s->crc32 = xz_crc32(b->in + s->in_start, in_used, s->crc32);
}


/*
 * Decode the Number of Records, Unpadded Size, and Uncompressed Size
 * fields from the Index field. That is, Index Padding and CRC32 are not
 * decoded by this function.
 *
 * This can return XZ_OK (more input needed), XZ_STREAM_END (everything
 * successfully decoded), or XZ_DATA_ERROR (input is corrupt).
 */
static int f_xz_dec_index(t_xz_dec *s, t_xz_buf *b)
{
    int ret;

    do
    {
        ret = f_xz_dec_vli(s, b->in, &b->in_pos, b->in_size);
        if (ret != XZ_STREAM_END)
        {
            f_xz_index_update(s, b);
            return ret;
        }

        switch (s->index.sequence)
        {
        case SEQ_INDEX_COUNT:
            s->index.count = s->vli;

            /*
             * Validate that the Number of Records field
             * indicates the same number of Records as
             * there were Blocks in the Stream.
             */
            if (s->index.count != s->block.count)
                return XZ_ERR_DATA;

            s->index.sequence = SEQ_INDEX_UNPADDED;
            break;

        case SEQ_INDEX_UNPADDED:
            s->index.hash.unpadded += s->vli;
            s->index.sequence = SEQ_INDEX_UNCOMPRESSED;
            break;

        case SEQ_INDEX_UNCOMPRESSED:
            s->index.hash.uncompressed += s->vli;
            s->index.hash.crc32 = xz_crc32(
                    (const uint8_t *)&s->index.hash,
                    sizeof(s->index.hash),
                    s->index.hash.crc32);
            --s->index.count;
            s->index.sequence = SEQ_INDEX_UNPADDED;
            break;
        }
    } while (s->index.count > 0);

    return XZ_STREAM_END;
}






static int f_xz_dec_main(t_xz_dec *s, t_xz_buf *b)
{
    int ret = XZ_OK;

    /*
     * Store the start position for the case when we are in the middle
     * of the Index field.
     */
    s->in_start = b->in_pos;

    for (;;)
    {
        switch (s->sequence)
        {
            case SEQ_STREAM_HEADER:
                /*
                 * Stream Header is copied to s->temp, and then
                 * decoded from there. This way if the caller
                 * gives us only little input at a time, we can
                 * still keep the Stream Header decoding code
                 * simple. Similar approach is used in many places
                 * in this file.
                 */
                if (!f_xz_fill_temp(s, b))
                    return XZ_OK;

                /*
                 * If dec_stream_header() returns
                 * XZ_UNSUPPORTED_CHECK, it is still possible
                 * to continue decoding if working in multi-call
                 * mode. Thus, update s->sequence before calling
                 * dec_stream_header().
                 */
                s->sequence = SEQ_BLOCK_START;

                ret = f_xz_dec_stream_header(s);
                if (ret != XZ_OK)
                    return ret;

            case SEQ_BLOCK_START:
                /* We need one byte of input to continue. */
                if (b->in_pos == b->in_size)
                    return XZ_OK;

                /* See if this is the beginning of the Index field. */
                if (b->in[b->in_pos] == 0)
                {
                    s->in_start = b->in_pos++;
                    s->sequence = SEQ_INDEX;
                    break;
                }

                /*
                 * Calculate the size of the Block Header and
                 * prepare to decode it.
                 */
                s->block_header.size
                        = ((uint32_t)b->in[b->in_pos] + 1) * 4;

                s->temp.size = s->block_header.size;
                s->temp.pos = 0;
                s->sequence = SEQ_BLOCK_HEADER;


            case SEQ_BLOCK_HEADER:
                if (!f_xz_fill_temp(s, b))
                    return XZ_OK;

                ret = f_xz_dec_block_header(s);
                if (ret != XZ_OK)
                    return ret;

                s->sequence = SEQ_BLOCK_UNCOMPRESS;
            case SEQ_BLOCK_UNCOMPRESS:
                ret = f_xz_dec_block(s, b);
                if (ret != XZ_STREAM_END)
                        return ret;

                s->sequence = SEQ_BLOCK_PADDING;

            case SEQ_BLOCK_PADDING:
                /*
                 * Size of Compressed Data + Block Padding
                 * must be a multiple of four. We don't need
                 * s->block.compressed for anything else
                 * anymore, so we use it here to test the size
                 * of the Block Padding field.
                 */
                while (s->block.compressed & 3)
                {
                    if (b->in_pos == b->in_size)
                        return XZ_OK;

                    if (b->in[b->in_pos++] != 0)
                        return XZ_ERR_DATA;

                    ++s->block.compressed;
                }
                s->sequence = SEQ_BLOCK_CHECK;
            case SEQ_BLOCK_CHECK:
                if (s->check_type == XZ_CHECK_CRC32)
                {
                    ret = f_crc32_validate(s, b);
                    if (ret != XZ_STREAM_END)
                        return ret;
                }
#ifdef XZ_DEC_ANY_CHECK
                else if (!f_check_skip(s, b))
                {
                    return XZ_OK;
                }
#endif
                s->sequence = SEQ_BLOCK_START;
                break;
            case SEQ_INDEX:
                ret = f_xz_dec_index(s, b);
                if (ret != XZ_STREAM_END)
                    return ret;

                s->sequence = SEQ_INDEX_PADDING;
            case SEQ_INDEX_PADDING:
                while ((s->index.size + (b->in_pos - s->in_start)) & 3)
                {
                    if (b->in_pos == b->in_size)
                    {
                        f_xz_index_update(s, b);
                        return XZ_OK;
                    }

                    if (b->in[b->in_pos++] != 0)
                        return XZ_ERR_DATA;
                }

                /* Finish the CRC32 value and Index size. */
                f_xz_index_update(s, b);

                /* Compare the hashes to validate the Index field. */
                if (memcmp(&s->block.hash, &s->index.hash,
                                sizeof(s->block.hash))!=0)
                    return XZ_ERR_DATA;

                s->sequence = SEQ_INDEX_CRC32;

            case SEQ_INDEX_CRC32:
                    ret = f_crc32_validate(s, b);
                    if (ret != XZ_STREAM_END)
                        return ret;

                    s->temp.size = STREAM_HEADER_SIZE;
                    s->sequence = SEQ_STREAM_FOOTER;

            case SEQ_STREAM_FOOTER:
                if (!f_xz_fill_temp(s, b))
                        return XZ_OK;

                return f_xz_dec_stream_footer(s);
            }
    }

    /* Never reached */
}









int xz_dec_run(t_xz_dec *s, t_xz_buf *b)
{
       /*size_t in_start;
       size_t out_start;*/
       int ret;

       /*in_start = b->in_pos;
       out_start = b->out_pos;*/
       ret = f_xz_dec_main(s, b);


       /*if (ret == XZ_OK && in_start == b->in_pos
                       && out_start == b->out_pos) {
               if (s->allow_buf_error)
                       ret = XZ_BUF_ERROR;

               s->allow_buf_error = true;
       } else {
               s->allow_buf_error = false;
       }*/

       return ret;
}


void xz_dec_reset(t_xz_dec *s)
{
    s->sequence = SEQ_STREAM_HEADER;
    s->pos = 0;
    s->crc32 = 0;
    memset(&s->block, 0, sizeof(s->block));
    memset(&s->index, 0, sizeof(s->index));
    s->temp.pos = 0;
    s->temp.size = STREAM_HEADER_SIZE;
}
