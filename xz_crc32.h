#ifndef XZ_CRC32_H
#define XZ_CRC32_H

#include "stdint.h"

#include "xz.h"

void xz_crc32_init(void);
uint32_t xz_crc32(const uint8_t *buf, size_t size, uint32_t crc);

#endif // XZ_CRC32_H
