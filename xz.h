#ifndef XZ_H
#define XZ_H

#include "typedefs.h"
#include "lcspec.h"
#include "xz_conf.h"

#include <string.h>



#define XZ_OK                      0
#define XZ_STREAM_END              1
#define XZ_ERR_UNSUPPORTED_CHECK  -1
#define XZ_ERR_UNSUP_DICT_SIZE    -2
#define XZ_ERR_FORMAT             -3
#define XZ_ERR_OPTIONS            -4
#define XZ_ERR_DATA               -5
#define XZ_ERR_CRC                -6



#define XZ_CHECK_NONE    0
#define XZ_CHECK_CRC32   1
#define XZ_CHECK_CRC64   4
#define XZ_CHECK_SHA256  10



/**
 * struct xz_buf - Passing input and output buffers to XZ code
 * @in:         Beginning of the input buffer. This may be NULL if and only
 *              if in_pos is equal to in_size.
 * @in_pos:     Current position in the input buffer. This must not exceed
 *              in_size.
 * @in_size:    Size of the input buffer
 * @out:        Beginning of the output buffer. This may be NULL if and only
 *              if out_pos is equal to out_size.
 * @out_pos:    Current position in the output buffer. This must not exceed
 *              out_size.
 * @out_size:   Size of the output buffer
 *
 * Only the contents of the output buffer from out[out_pos] onward, and
 * the variables in_pos and out_pos are modified by the XZ code.
 */
typedef struct  {
        const uint8_t *in;
        size_t in_pos;
        size_t in_size;

        uint8_t *out;
        size_t out_pos;
        size_t out_size;
} t_xz_buf;

#ifndef MIN
    #define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif






#endif // XZ_H
