/*
 * xz_conf.h
 *  Конфигурационный файл для библиотеки xz-компрессора для встраиваемых систем
 *
 *  Created on: 29.01.2011
 *      Author: Melkor
 */

#ifndef XZ_CONF_H_
#define XZ_CONF_H_

//максимальный размер словаря данных
#define LZMA_DICT_MAX_SIZE       (4*1024)
//максимальные значения для параметров алгоритма LZMA
#define XZ_LZMA_LC_MAX            0
#define XZ_LZMA_PB_MAX            0
#define XZ_LZMA_LP_MAX            0

#endif /* XZ_CONF_H_ */
