#ifndef XZ_LZMA2_H
#define XZ_LZMA2_H

#include "xz.h"

/* Range coder constants */
#define RC_SHIFT_BITS 8
#define RC_TOP_BITS 24
#define RC_TOP_VALUE (1 << RC_TOP_BITS)
#define RC_BIT_MODEL_TOTAL_BITS 11
#define RC_BIT_MODEL_TOTAL (1 << RC_BIT_MODEL_TOTAL_BITS)
#define RC_MOVE_BITS 5


#if (XZ_LZMA_PB_MAX < 0) || (XZ_LZMA_PB_MAX > 4)
    #error "invalid XZ_LZMA_PB_MAX value (must be frotm 0 to 4)"
#endif

/*
 * Maximum number of position states. A position state is the lowest pb
 * number of bits of the current uncompressed offset. In some places there
 * are different sets of probabilities for different position states.
 */
#define POS_STATES_MAX (1 << XZ_LZMA_PB_MAX)

/*
 * This enum is used to track which LZMA symbols have occurred most recently
 * and in which order. This information is used to predict the next symbol.
 *
 * Symbols:
 *  - Literal: One 8-bit byte
 *  - Match: Repeat a chunk of data at some distance
 *  - Long repeat: Multi-byte match at a recently seen distance
 *  - Short repeat: One-byte repeat at a recently seen distance
 *
 * The symbol names are in from STATE_oldest_older_previous. REP means
 * either short or long repeated match, and NONLIT means any non-literal.
 */
enum lzma_state {
        STATE_LIT_LIT,
        STATE_MATCH_LIT_LIT,
        STATE_REP_LIT_LIT,
        STATE_SHORTREP_LIT_LIT,
        STATE_MATCH_LIT,
        STATE_REP_LIT,
        STATE_SHORTREP_LIT,
        STATE_LIT_MATCH,
        STATE_LIT_LONGREP,
        STATE_LIT_SHORTREP,
        STATE_NONLIT_MATCH,
        STATE_NONLIT_REP
};

/* Total number of states */
#define STATES 12

/* The lowest 7 states indicate that the previous state was a literal. */
#define LIT_STATES 7

/* Indicate that the latest symbol was a literal. */
static LINLINE void f_lzma_state_literal(enum lzma_state *state)
{
    if (*state <= STATE_SHORTREP_LIT_LIT)
       *state = STATE_LIT_LIT;
    else if (*state <= STATE_LIT_SHORTREP)
        *state -= 3;
    else
        *state -= 6;
}

/* Indicate that the latest symbol was a match. */
static LINLINE void f_lzma_state_match(enum lzma_state *state)
{
    *state = *state < LIT_STATES ? STATE_LIT_MATCH : STATE_NONLIT_MATCH;
}

/* Indicate that the latest state was a long repeated match. */
static LINLINE void  f_lzma_state_long_rep(enum lzma_state *state)
{
    *state = *state < LIT_STATES ? STATE_LIT_LONGREP : STATE_NONLIT_REP;
}

/* Indicate that the latest symbol was a short match. */
static LINLINE void  f_lzma_state_short_rep(enum lzma_state *state)
{
    *state = *state < LIT_STATES ? STATE_LIT_SHORTREP : STATE_NONLIT_REP;
}

/* Test if the previous symbol was a literal. */
static LINLINE int  f_lzma_state_is_literal(enum lzma_state state)
{
    return state < LIT_STATES;
}

/* Each literal coder is divided in three sections:
 *   - 0x001-0x0FF: Without match byte
 *   - 0x101-0x1FF: With match byte; match bit is 0
 *   - 0x201-0x2FF: With match byte; match bit is 1
 *
 * Match byte is used when the previous LZMA symbol was something else than
 * a literal (that is, it was some kind of match).
 */
#define LITERAL_CODER_SIZE 0x300

#if (XZ_LZMA_LC_MAX < 0) || (XZ_LZMA_LC_MAX > 4)
    #error "invalid XZ_LZMA_LC_MAX value (must be frotm 0 to 4)"
#endif

/* Maximum number of literal coders */
#define LITERAL_CODERS_MAX (1 << XZ_LZMA_LC_MAX)

/* Minimum length of a match is two bytes. */
#define MATCH_LEN_MIN 2

/* Match length is encoded with 4, 5, or 10 bits.
 *
 * Length   Bits
 *  2-9      4 = Choice=0 + 3 bits
 * 10-17     5 = Choice=1 + Choice2=0 + 3 bits
 * 18-273   10 = Choice=1 + Choice2=1 + 8 bits
 */
#define LEN_LOW_BITS 3
#define LEN_LOW_SYMBOLS (1 << LEN_LOW_BITS)
#define LEN_MID_BITS 3
#define LEN_MID_SYMBOLS (1 << LEN_MID_BITS)
#define LEN_HIGH_BITS 8
#define LEN_HIGH_SYMBOLS (1 << LEN_HIGH_BITS)
#define LEN_SYMBOLS (LEN_LOW_SYMBOLS + LEN_MID_SYMBOLS + LEN_HIGH_SYMBOLS)

/*
 * Maximum length of a match is 273 which is a result of the encoding
 * described above.
 */
#define MATCH_LEN_MAX (MATCH_LEN_MIN + LEN_SYMBOLS - 1)

/*
 * Different sets of probabilities are used for match distances that have
 * very short match length: Lengths of 2, 3, and 4 bytes have a separate
 * set of probabilities for each length. The matches with longer length
 * use a shared set of probabilities.
 */
#define DIST_STATES 4

/*
 * Get the index of the appropriate probability array for decoding
 * the distance slot.
 */
static LINLINE uint32_t f_lzma_get_dist_state(uint32_t len)
{
    return len < DIST_STATES + MATCH_LEN_MIN
                    ? len - MATCH_LEN_MIN : DIST_STATES - 1;
}

/*
 * The highest two bits of a 32-bit match distance are encoded using six bits.
 * This six-bit value is called a distance slot. This way encoding a 32-bit
 * value takes 6-36 bits, larger values taking more bits.
 */
#define DIST_SLOT_BITS 6
#define DIST_SLOTS (1 << DIST_SLOT_BITS)

/* Match distances up to 127 are fully encoded using probabilities. Since
 * the highest two bits (distance slot) are always encoded using six bits,
 * the distances 0-3 don't need any additional bits to encode, since the
 * distance slot itself is the same as the actual distance. DIST_MODEL_START
 * indicates the first distance slot where at least one additional bit is
 * needed.
 */
#define DIST_MODEL_START 4

/*
 * Match distances greater than 127 are encoded in three pieces:
 *   - distance slot: the highest two bits
 *   - direct bits: 2-26 bits below the highest two bits
 *   - alignment bits: four lowest bits
 *
 * Direct bits don't use any probabilities.
 *
 * The distance slot value of 14 is for distances 128-191.
 */
#define DIST_MODEL_END 14

/* Distance slots that indicate a distance <= 127. */
#define FULL_DISTANCES_BITS (DIST_MODEL_END / 2)
#define FULL_DISTANCES (1 << FULL_DISTANCES_BITS)

/*
 * For match distances greater than 127, only the highest two bits and the
 * lowest four bits (alignment) is encoded using probabilities.
 */
#define ALIGN_BITS 4
#define ALIGN_SIZE (1 << ALIGN_BITS)
#define ALIGN_MASK (ALIGN_SIZE - 1)

/* Total number of all probability variables */
#define PROBS_TOTAL (1846 + LITERAL_CODERS_MAX * LITERAL_CODER_SIZE)

/*
 * LZMA remembers the four most recent match distances. Reusing these
 * distances tends to take less space than re-encoding the actual
 * distance value.
 */
#define REPS 4










/*
 * Range decoder initialization eats the first five bytes of each LZMA chunk.
 */
#define RC_INIT_BYTES 5

/*
 * Minimum number of usable input buffer to safely decode one LZMA symbol.
 * The worst case is that we decode 22 bits using probabilities and 26
 * direct bits. This may decode at maximum of 20 bytes of input. However,
 * lzma_main() does an extra normalization before returning, thus we
 * need to put 21 here.
 */
#define LZMA_IN_REQUIRED 21

/*
 * Dictionary (history buffer)
 *
 * These are always true:
 *    start <= pos <= full <= end
 *    pos <= limit <= end
 *
 * In multi-call mode, also these are true:
 *    end == size
 *    size <= size_max
 *    allocated <= size
 *
 * Most of these variables are size_t to support single-call mode,
 * in which the dictionary variables address the actual output
 * buffer directly.
 */
typedef struct
{
    /* Beginning of the history buffer */
    uint8_t buf[LZMA_DICT_MAX_SIZE];

    /* Old position in buf (before decoding more data) */
    size_t start;

    /* Position in buf */
    size_t pos;

    /*
     * How full dictionary is. This is used to detect corrupt input that
     * would read beyond the beginning of the uncompressed stream.
     */
    size_t full;

    /* Write limit; we don't write to buf[limit] or later bytes. */
    size_t limit;

    /*
     * End of the dictionary buffer. In multi-call mode, this is
     * the same as the dictionary size. In single-call mode, this
     * indicates the size of the output buffer.
     */
    size_t end;

    /*
     * Size of the dictionary as specified in Block Header. This is used
     * together with "full" to detect corrupt input that would make us
     * read beyond the beginning of the uncompressed stream.
     */
    uint32_t size;
} t_xz_lzma_dict;

/* Range decoder */
typedef struct
{
    uint32_t range;
    uint32_t code;

    /*
     * Number of initializing bytes remaining to be read
     * by rc_read_init().
     */
    uint32_t init_bytes_left;

    /*
     * Buffer from which we read our input. It can be either
     * temp.buf or the caller-provided input buffer.
     */
    const uint8_t *in;
    size_t in_pos;
    size_t in_limit;
} t_rc_dec;

/* Probabilities for a length decoder. */
typedef struct
{
    /* Probability of match length being at least 10 */
    uint16_t choice;

    /* Probability of match length being at least 18 */
    uint16_t choice2;

    /* Probabilities for match lengths 2-9 */
    uint16_t low[POS_STATES_MAX][LEN_LOW_SYMBOLS];

    /* Probabilities for match lengths 10-17 */
    uint16_t mid[POS_STATES_MAX][LEN_MID_SYMBOLS];

    /* Probabilities for match lengths 18-273 */
    uint16_t high[LEN_HIGH_SYMBOLS];
} t_lzma_len_dec;

typedef struct
{
    /* Distances of latest four matches */
    uint32_t rep0;
    uint32_t rep1;
    uint32_t rep2;
    uint32_t rep3;

    /* Types of the most recently seen LZMA symbols */
    enum lzma_state state;

    /*
     * Length of a match. This is updated so that dict_repeat can
     * be called again to finish repeating the whole match.
     */
    uint32_t len;

    /*
     * LZMA properties or related bit masks (number of literal
     * context bits, a mask dervied from the number of literal
     * position bits, and a mask dervied from the number
     * position bits)
     */
    uint32_t lc;
    uint32_t literal_pos_mask; /* (1 << lp) - 1 */
    uint32_t pos_mask;         /* (1 << pb) - 1 */

    /* If 1, it's a match. Otherwise it's a single 8-bit literal. */
    uint16_t is_match[STATES][POS_STATES_MAX];

    /* If 1, it's a repeated match. The distance is one of rep0 .. rep3. */
    uint16_t is_rep[STATES];

    /*
     * If 0, distance of a repeated match is rep0.
     * Otherwise check is_rep1.
     */
    uint16_t is_rep0[STATES];

    /*
     * If 0, distance of a repeated match is rep1.
     * Otherwise check is_rep2.
     */
    uint16_t is_rep1[STATES];

    /* If 0, distance of a repeated match is rep2. Otherwise it is rep3. */
    uint16_t is_rep2[STATES];

    /*
     * If 1, the repeated match has length of one byte. Otherwise
     * the length is decoded from rep_len_decoder.
     */
    uint16_t is_rep0_long[STATES][POS_STATES_MAX];

    /*
     * Probability tree for the highest two bits of the match
     * distance. There is a separate probability tree for match
     * lengths of 2 (i.e. MATCH_LEN_MIN), 3, 4, and [5, 273].
     */
    uint16_t dist_slot[DIST_STATES][DIST_SLOTS];

    /*
     * Probility trees for additional bits for match distance
     * when the distance is in the range [4, 127].
     */
    uint16_t dist_special[FULL_DISTANCES - DIST_MODEL_END];

    /*
     * Probability tree for the lowest four bits of a match
     * distance that is equal to or greater than 128.
     */
    uint16_t dist_align[ALIGN_SIZE];

    /* Length of a normal match */
    t_lzma_len_dec match_len_dec;

    /* Length of a repeated match */
    t_lzma_len_dec rep_len_dec;

    /* Probabilities of literals */
    uint16_t literal[LITERAL_CODERS_MAX][LITERAL_CODER_SIZE];
} t_lzma_dec;

typedef struct
{
    /* Position in xz_dec_lzma2_run(). */
    enum lzma2_seq {
            SEQ_CONTROL,
            SEQ_UNCOMPRESSED_1,
            SEQ_UNCOMPRESSED_2,
            SEQ_COMPRESSED_0,
            SEQ_COMPRESSED_1,
            SEQ_PROPERTIES,
            SEQ_LZMA_PREPARE,
            SEQ_LZMA_RUN,
            SEQ_COPY
    } sequence;

    /* Next position after decoding the compressed size of the chunk. */
    enum lzma2_seq next_sequence;

    /* Uncompressed size of LZMA chunk (2 MiB at maximum) */
    uint32_t uncompressed;

    /*
     * Compressed size of LZMA chunk or compressed/uncompressed
     * size of uncompressed chunk (64 KiB at maximum)
     */
    uint32_t compressed;

    /*
     * True if dictionary reset is needed. This is false before
     * the first chunk (LZMA or uncompressed).
     */
    uint8_t need_dict_reset;

    /*
     * True if new LZMA properties are needed. This is false
     * before the first LZMA chunk.
     */
    uint8_t need_props;
} t_lzma2_dec;

typedef struct
{
    /*
     * The order below is important on x86 to reduce code size and
     * it shouldn't hurt on other platforms. Everything up to and
     * including lzma.pos_mask are in the first 128 bytes on x86-32,
     * which allows using smaller instructions to access those
     * variables. On x86-64, fewer variables fit into the first 128
     * bytes, but this is still the best order without sacrificing
     * the readability by splitting the structures.
     */
    t_lzma2_dec lzma2;
    t_lzma_dec lzma;

    t_rc_dec rc;
    t_xz_lzma_dict dict;


    /*
     * Temporary buffer which holds small number of input bytes between
     * decoder calls. See lzma2_lzma() for details.
     */
    struct
    {
        uint32_t size;
        uint8_t buf[3 * LZMA_IN_REQUIRED];
    } temp;
} t_xz_dec_lzma2;


int xz_dec_lzma2_reset(t_xz_dec_lzma2 *s, uint8_t props);
int xz_dec_lzma2_run(t_xz_dec_lzma2 *s, t_xz_buf *b);

#endif // XZ_LZMA2_H
