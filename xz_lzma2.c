#include "xz_lzma2.h"



/**************
 * Dictionary *
 **************/

/*
 * Reset the dictionary state. When in single-call mode, set up the beginning
 * of the dictionary to point to the actual output buffer.
 */
static void f_dict_reset(t_xz_lzma_dict *dict, t_xz_buf *b)
{
    dict->start = 0;
    dict->pos = 0;
    dict->limit = 0;
    dict->full = 0;
}

/* Set dictionary write limit */
static void f_dict_limit(t_xz_lzma_dict *dict, size_t out_max)
{
    if (dict->end - dict->pos <= out_max)
        dict->limit = dict->end;
    else
        dict->limit = dict->pos + out_max;
}

/* Return true if at least one byte can be written into the dictionary. */
static int f_dict_has_space(const t_xz_lzma_dict *dict)
{
    return (dict->pos < dict->limit);
}

/*
 * Get a byte from the dictionary at the given distance. The distance is
 * assumed to valid, or as a special case, zero when the dictionary is
 * still empty. This special case is needed for single-call decoding to
 * avoid writing a '\0' to the end of the destination buffer.
 */
static uint32_t f_dict_get(const t_xz_lzma_dict *dict, uint32_t dist)
{
    size_t offset = dict->pos - dist - 1;

    if (dist >= dict->pos)
        offset += dict->end;

    return dict->full > 0 ? dict->buf[offset] : 0;
}

/*
 * Put one byte into the dictionary. It is assumed that there is space for it.
 */
static void f_dict_put(t_xz_lzma_dict *dict, uint8_t byte)
{
    dict->buf[dict->pos++] = byte;

    if (dict->full < dict->pos)
        dict->full = dict->pos;
}

/*
 * Repeat given number of bytes from the given distance. If the distance is
 * invalid, false is returned. On success, true is returned and *len is
 * updated to indicate how many bytes were left to be repeated.
 */
static int f_dict_repeat(t_xz_lzma_dict *dict, uint32_t *len, uint32_t dist)
{
    size_t back;
    uint32_t left;

    if (dist >= dict->full || dist >= dict->size)
        return FALSE;

    left = MIN(dict->limit - dict->pos, *len);
    *len -= left;

    back = dict->pos - dist - 1;
    if (dist >= dict->pos)
        back += dict->end;

    do
    {
        dict->buf[dict->pos++] = dict->buf[back++];
        if (back == dict->end)
            back = 0;
    } while (--left > 0);

    if (dict->full < dict->pos)
        dict->full = dict->pos;

    return TRUE;
}

/* Copy uncompressed data as is from input to dictionary and output buffers. */
static void f_dict_uncompressed(t_xz_lzma_dict *dict, t_xz_buf *b, uint32_t *left)
{
    size_t copy_size;

    while (*left > 0 && b->in_pos < b->in_size
            && b->out_pos < b->out_size)
    {
        copy_size = MIN(b->in_size - b->in_pos,
                b->out_size - b->out_pos);
        if (copy_size > dict->end - dict->pos)
            copy_size = dict->end - dict->pos;
        if (copy_size > *left)
            copy_size = *left;

        *left -= copy_size;

        memcpy(dict->buf + dict->pos, b->in + b->in_pos, copy_size);
        dict->pos += copy_size;

        if (dict->full < dict->pos)
            dict->full = dict->pos;


        if (dict->pos == dict->end)
            dict->pos = 0;

        memcpy(b->out + b->out_pos, b->in + b->in_pos,
                copy_size);


        dict->start = dict->pos;

        b->out_pos += copy_size;
        b->in_pos += copy_size;
    }
}

/*
 * Flush pending data from dictionary to b->out. It is assumed that there is
 * enough space in b->out. This is guaranteed because caller uses dict_limit()
 * before decoding data into the dictionary.
 */
static uint32_t f_dict_flush(t_xz_lzma_dict *dict, t_xz_buf *b)
{
    size_t copy_size = dict->pos - dict->start;

    if (dict->pos == dict->end)
        dict->pos = 0;

    memcpy(b->out + b->out_pos, dict->buf + dict->start, copy_size);

    dict->start = dict->pos;
    b->out_pos += copy_size;
    return copy_size;
}


/*****************
 * Range decoder *
 *****************/

/* Reset the range decoder. */
static void f_rc_reset(t_rc_dec *rc)
{
    rc->range = (uint32_t)-1;
    rc->code = 0;
    rc->init_bytes_left = RC_INIT_BYTES;
}

/*
 * Read the first five initial bytes into rc->code if they haven't been
 * read already. (Yes, the first byte gets completely ignored.)
 */
static int f_rc_read_init(t_rc_dec *rc, t_xz_buf *b)
{
    while (rc->init_bytes_left > 0)
    {
        if (b->in_pos == b->in_size)
            return FALSE;

        rc->code = (rc->code << 8) + b->in[b->in_pos++];
        --rc->init_bytes_left;
    }

    return TRUE;
}

/* Return true if there may not be enough input for the next decoding loop. */
static LINLINE int f_rc_limit_exceeded(const t_rc_dec *rc)
{
    return rc->in_pos > rc->in_limit;
}

/*
 * Return true if it is possible (from point of view of range decoder) that
 * we have reached the end of the LZMA chunk.
 */
static LINLINE int f_rc_is_finished(const t_rc_dec *rc)
{
    return rc->code == 0;
}

/* Read the next input byte if needed. */
static void f_rc_normalize(t_rc_dec *rc)
{
    if (rc->range < RC_TOP_VALUE)
    {
        rc->range <<= RC_SHIFT_BITS;
        rc->code = (rc->code << RC_SHIFT_BITS) + rc->in[rc->in_pos++];
    }
}

/*
 * Decode one bit. In some versions, this function has been splitted in three
 * functions so that the compiler is supposed to be able to more easily avoid
 * an extra branch. In this particular version of the LZMA decoder, this
 * doesn't seem to be a good idea (tested with GCC 3.3.6, 3.4.6, and 4.3.3
 * on x86). Using a non-splitted version results in nicer looking code too.
 *
 * NOTE: This must return an int. Do not make it return a bool or the speed
 * of the code generated by GCC 3.x decreases 10-15 %. (GCC 4.3 doesn't care,
 * and it generates 10-20 % faster code than GCC 3.x from this file anyway.)
 */
static int f_rc_bit(t_rc_dec *rc, uint16_t *prob)
{
    uint32_t bound;
    int bit;

    f_rc_normalize(rc);
    bound = (rc->range >> RC_BIT_MODEL_TOTAL_BITS) * *prob;
    if (rc->code < bound)
    {
        rc->range = bound;
        *prob += (RC_BIT_MODEL_TOTAL - *prob) >> RC_MOVE_BITS;
        bit = 0;
    }
    else
    {
        rc->range -= bound;
        rc->code -= bound;
        *prob -= *prob >> RC_MOVE_BITS;
        bit = 1;
    }

    return bit;
}

/* Decode a bittree starting from the most significant bit. */
static uint32_t f_rc_bittree(t_rc_dec *rc, uint16_t *probs,
                                     uint32_t limit)
{
    uint32_t symbol = 1;

    do
    {
        if (f_rc_bit(rc, &probs[symbol]))
            symbol = (symbol << 1) + 1;
        else
            symbol <<= 1;
    } while (symbol < limit);

    return symbol;
}

/* Decode a bittree starting from the least significant bit. */
static void f_rc_bittree_reverse(t_rc_dec *rc,
        uint16_t *probs, uint32_t *dest, uint32_t limit)
{
    uint32_t symbol = 1;
    uint32_t i = 0;

    do
    {
        if (f_rc_bit(rc, &probs[symbol]))
        {
            symbol = (symbol << 1) + 1;
            *dest += 1 << i;
        } else
        {
            symbol <<= 1;
        }
    } while (++i < limit);
}

/* Decode direct bits (fixed fifty-fifty probability) */
static LINLINE void f_rc_direct(t_rc_dec *rc, uint32_t *dest, uint32_t limit)
{
    uint32_t mask;

    do
    {
        f_rc_normalize(rc);
        rc->range >>= 1;
        rc->code -= rc->range;
        mask = (uint32_t)0 - (rc->code >> 31);
        rc->code += rc->range & mask;
        *dest = (*dest << 1) + (mask + 1);
    } while (--limit > 0);
}


/********
 * LZMA *
 ********/

/* Get pointer to literal coder probability array. */
static uint16_t *f_lzma_literal_probs(t_xz_dec_lzma2 *s)
{
    uint32_t prev_byte = f_dict_get(&s->dict, 0);
    uint32_t low = prev_byte >> (8 - s->lzma.lc);
    uint32_t high = (s->dict.pos & s->lzma.literal_pos_mask) << s->lzma.lc;
    return s->lzma.literal[low + high];
}

/* Decode a literal (one 8-bit byte) */
static void f_lzma_literal(t_xz_dec_lzma2 *s)
{
    uint16_t *probs;
    uint32_t symbol;
    uint32_t match_byte;
    uint32_t match_bit;
    uint32_t offset;
    uint32_t i;

    probs = f_lzma_literal_probs(s);

    if (f_lzma_state_is_literal(s->lzma.state))
    {
        symbol = f_rc_bittree(&s->rc, probs, 0x100);
    }
    else
    {
        symbol = 1;
        match_byte = f_dict_get(&s->dict, s->lzma.rep0) << 1;
        offset = 0x100;

        do
        {
            match_bit = match_byte & offset;
            match_byte <<= 1;
            i = offset + match_bit + symbol;

            if (f_rc_bit(&s->rc, &probs[i]))
            {
                symbol = (symbol << 1) + 1;
                offset &= match_bit;
            }
            else
            {
                symbol <<= 1;
                offset &= ~match_bit;
            }
        } while (symbol < 0x100);
    }

    f_dict_put(&s->dict, (uint8_t)symbol);
    f_lzma_state_literal(&s->lzma.state);
}

/* Decode the length of the match into s->lzma.len. */
static void f_lzma_len(t_xz_dec_lzma2 *s, t_lzma_len_dec *l,
        uint32_t pos_state)
{
    uint16_t *probs;
    uint32_t limit;

    if (!f_rc_bit(&s->rc, &l->choice))
    {
        probs = l->low[pos_state];
        limit = LEN_LOW_SYMBOLS;
        s->lzma.len = MATCH_LEN_MIN;
    }
    else
    {
        if (!f_rc_bit(&s->rc, &l->choice2))
        {
            probs = l->mid[pos_state];
            limit = LEN_MID_SYMBOLS;
            s->lzma.len = MATCH_LEN_MIN + LEN_LOW_SYMBOLS;
        }
        else
        {
            probs = l->high;
            limit = LEN_HIGH_SYMBOLS;
            s->lzma.len = MATCH_LEN_MIN + LEN_LOW_SYMBOLS
                    + LEN_MID_SYMBOLS;
        }
    }

    s->lzma.len += f_rc_bittree(&s->rc, probs, limit) - limit;
}

/* Decode a match. The distance will be stored in s->lzma.rep0. */
static void f_lzma_match(t_xz_dec_lzma2 *s, uint32_t pos_state)
{
    uint16_t *probs;
    uint32_t dist_slot;
    uint32_t limit;

    f_lzma_state_match(&s->lzma.state);

    s->lzma.rep3 = s->lzma.rep2;
    s->lzma.rep2 = s->lzma.rep1;
    s->lzma.rep1 = s->lzma.rep0;

    f_lzma_len(s, &s->lzma.match_len_dec, pos_state);

    probs = s->lzma.dist_slot[f_lzma_get_dist_state(s->lzma.len)];
    dist_slot = f_rc_bittree(&s->rc, probs, DIST_SLOTS) - DIST_SLOTS;

    if (dist_slot < DIST_MODEL_START)
    {
        s->lzma.rep0 = dist_slot;
    }
    else
    {
        limit = (dist_slot >> 1) - 1;
        s->lzma.rep0 = 2 + (dist_slot & 1);

        if (dist_slot < DIST_MODEL_END)
        {
            s->lzma.rep0 <<= limit;
            probs = s->lzma.dist_special + s->lzma.rep0
                    - dist_slot - 1;
            f_rc_bittree_reverse(&s->rc, probs,
                    &s->lzma.rep0, limit);
        }
        else
        {
            f_rc_direct(&s->rc, &s->lzma.rep0, limit - ALIGN_BITS);
            s->lzma.rep0 <<= ALIGN_BITS;
            f_rc_bittree_reverse(&s->rc, s->lzma.dist_align,
                    &s->lzma.rep0, ALIGN_BITS);
        }
    }
}

/*
 * Decode a repeated match. The distance is one of the four most recently
 * seen matches. The distance will be stored in s->lzma.rep0.
 */
static void f_lzma_rep_match(t_xz_dec_lzma2 *s, uint32_t pos_state)
{
    uint32_t tmp;

    if (!f_rc_bit(&s->rc, &s->lzma.is_rep0[s->lzma.state]))
    {
        if (!f_rc_bit(&s->rc, &s->lzma.is_rep0_long[
                s->lzma.state][pos_state]))
        {
            f_lzma_state_short_rep(&s->lzma.state);
            s->lzma.len = 1;
            return;
        }
    }
    else
    {
        if (!f_rc_bit(&s->rc, &s->lzma.is_rep1[s->lzma.state]))
        {
            tmp = s->lzma.rep1;
        }
        else
        {
            if (!f_rc_bit(&s->rc, &s->lzma.is_rep2[s->lzma.state]))
            {
                tmp = s->lzma.rep2;
            }
            else
            {
                tmp = s->lzma.rep3;
                s->lzma.rep3 = s->lzma.rep2;
            }

            s->lzma.rep2 = s->lzma.rep1;
        }

        s->lzma.rep1 = s->lzma.rep0;
        s->lzma.rep0 = tmp;
    }

    f_lzma_state_long_rep(&s->lzma.state);
    f_lzma_len(s, &s->lzma.rep_len_dec, pos_state);
}

/* LZMA decoder core */
static int f_lzma_main(t_xz_dec_lzma2 *s)
{
    uint32_t pos_state;

    /*
     * If the dictionary was reached during the previous call, try to
     * finish the possibly pending repeat in the dictionary.
     */
    if (f_dict_has_space(&s->dict) && s->lzma.len > 0)
        f_dict_repeat(&s->dict, &s->lzma.len, s->lzma.rep0);

    /*
     * Decode more LZMA symbols. One iteration may consume up to
     * LZMA_IN_REQUIRED - 1 bytes.
     */
    while (f_dict_has_space(&s->dict) && !f_rc_limit_exceeded(&s->rc))
    {
        pos_state = s->dict.pos & s->lzma.pos_mask;

        if (!f_rc_bit(&s->rc, &s->lzma.is_match[
                s->lzma.state][pos_state]))
        {
            f_lzma_literal(s);
        }
        else
        {
            if (f_rc_bit(&s->rc, &s->lzma.is_rep[s->lzma.state]))
                f_lzma_rep_match(s, pos_state);
            else
                f_lzma_match(s, pos_state);

            if (!f_dict_repeat(&s->dict, &s->lzma.len, s->lzma.rep0))
                return FALSE;
        }
    }

    /*
     * Having the range decoder always normalized when we are outside
     * this function makes it easier to correctly handle end of the chunk.
     */
    f_rc_normalize(&s->rc);

    return TRUE;
}

/*
 * Reset the LZMA decoder and range decoder state. Dictionary is nore reset
 * here, because LZMA state may be reset without resetting the dictionary.
 */
static void f_lzma_reset(t_xz_dec_lzma2 *s)
{
    uint16_t *probs;
    size_t i;

    s->lzma.state = STATE_LIT_LIT;
    s->lzma.rep0 = 0;
    s->lzma.rep1 = 0;
    s->lzma.rep2 = 0;
    s->lzma.rep3 = 0;

    /*
     * All probabilities are initialized to the same value. This hack
     * makes the code smaller by avoiding a separate loop for each
     * probability array.
     *
     * This could be optimized so that only that part of literal
     * probabilities that are actually required. In the common case
     * we would write 12 KiB less.
     */
    probs = s->lzma.is_match[0];
    for (i = 0; i < PROBS_TOTAL; ++i)
        probs[i] = RC_BIT_MODEL_TOTAL / 2;

    f_rc_reset(&s->rc);
}

/*
 * Decode and validate LZMA properties (lc/lp/pb) and calculate the bit masks
 * from the decoded lp and pb values. On success, the LZMA decoder state is
 * reset and true is returned.
 */
static int f_lzma_props(t_xz_dec_lzma2 *s, uint8_t props)
{
    if (props > (4 * 5 + 4) * 9 + 8)
        return FALSE;

    s->lzma.pos_mask = 0;
    while (props >= 9 * 5)
    {
        props -= 9 * 5;
        ++s->lzma.pos_mask;
    }

    if (s->lzma.pos_mask > XZ_LZMA_PB_MAX)
        return FALSE;

    s->lzma.pos_mask = (1 << s->lzma.pos_mask) - 1;

    s->lzma.literal_pos_mask = 0;
    while (props >= 9)
    {
        props -= 9;
        ++s->lzma.literal_pos_mask;
    }

    s->lzma.lc = props;

    if (s->lzma.lc > XZ_LZMA_LC_MAX)
        return FALSE;

    if (s->lzma.lc + s->lzma.literal_pos_mask > 4)
        return FALSE;

    if (s->lzma.literal_pos_mask > XZ_LZMA_LP_MAX)
        return FALSE;

    s->lzma.literal_pos_mask = (1 << s->lzma.literal_pos_mask) - 1;

    f_lzma_reset(s);

    return TRUE;
}



/*********
 * LZMA2 *
 *********/

/*
 * The LZMA decoder assumes that if the input limit (s->rc.in_limit) hasn't
 * been exceeded, it is safe to read up to LZMA_IN_REQUIRED bytes. This
 * wrapper function takes care of making the LZMA decoder's assumption safe.
 *
 * As long as there is plenty of input left to be decoded in the current LZMA
 * chunk, we decode directly from the caller-supplied input buffer until
 * there's LZMA_IN_REQUIRED bytes left. Those remaining bytes are copied into
 * s->temp.buf, which (hopefully) gets filled on the next call to this
 * function. We decode a few bytes from the temporary buffer so that we can
 * continue decoding from the caller-supplied input buffer again.
 */
static int f_lzma2_lzma(t_xz_dec_lzma2 *s, t_xz_buf *b)
{
    size_t in_avail;
    uint32_t tmp;

    in_avail = b->in_size - b->in_pos;
    if (s->temp.size > 0 || s->lzma2.compressed == 0)
    {
        tmp = 2 * LZMA_IN_REQUIRED - s->temp.size;
        if (tmp > s->lzma2.compressed - s->temp.size)
            tmp = s->lzma2.compressed - s->temp.size;
        if (tmp > in_avail)
            tmp = in_avail;

        memcpy(s->temp.buf + s->temp.size, b->in + b->in_pos, tmp);

        if (s->temp.size + tmp == s->lzma2.compressed)
        {
            memset(s->temp.buf + s->temp.size + tmp, 0,
                    sizeof(s->temp.buf)
                        - s->temp.size - tmp);
            s->rc.in_limit = s->temp.size + tmp;
        }
        else if (s->temp.size + tmp < LZMA_IN_REQUIRED)
        {
            s->temp.size += tmp;
            b->in_pos += tmp;
            return TRUE;
        }
        else
        {
            s->rc.in_limit = s->temp.size + tmp - LZMA_IN_REQUIRED;
        }

        s->rc.in = s->temp.buf;
        s->rc.in_pos = 0;

        if (!f_lzma_main(s) || s->rc.in_pos > s->temp.size + tmp)
            return FALSE;

        s->lzma2.compressed -= s->rc.in_pos;

        if (s->rc.in_pos < s->temp.size) {
            s->temp.size -= s->rc.in_pos;
            memmove(s->temp.buf, s->temp.buf + s->rc.in_pos,
                    s->temp.size);
            return TRUE;
        }

        b->in_pos += s->rc.in_pos - s->temp.size;
        s->temp.size = 0;
    }

    in_avail = b->in_size - b->in_pos;
    if (in_avail >= LZMA_IN_REQUIRED)
    {
        s->rc.in = b->in;
        s->rc.in_pos = b->in_pos;

        if (in_avail >= s->lzma2.compressed + LZMA_IN_REQUIRED)
            s->rc.in_limit = b->in_pos + s->lzma2.compressed;
        else
            s->rc.in_limit = b->in_size - LZMA_IN_REQUIRED;

        if (!f_lzma_main(s))
            return FALSE;

        in_avail = s->rc.in_pos - b->in_pos;
        if (in_avail > s->lzma2.compressed)
            return FALSE;

        s->lzma2.compressed -= in_avail;
        b->in_pos = s->rc.in_pos;
    }

    in_avail = b->in_size - b->in_pos;
    if (in_avail < LZMA_IN_REQUIRED)
    {
        if (in_avail > s->lzma2.compressed)
            in_avail = s->lzma2.compressed;

        memcpy(s->temp.buf, b->in + b->in_pos, in_avail);
        s->temp.size = in_avail;
        b->in_pos += in_avail;
    }

    return TRUE;
}



int xz_dec_lzma2_reset(t_xz_dec_lzma2 *s, uint8_t props)
{
    /* This limits dictionary size to 3 GiB to keep parsing simpler. */
    /* This limits dictionary size to 3 GiB to keep parsing simpler. */
    if (props > 39)
        return XZ_ERR_OPTIONS;

    s->dict.size = 2 + (props & 1);
    s->dict.size <<= (props >> 1) + 11;

    if (s->dict.size > LZMA_DICT_MAX_SIZE)
    {
        return XZ_ERR_UNSUP_DICT_SIZE;
    }

    s->dict.end = s->dict.size;

    s->lzma.len = 0;

    s->lzma2.sequence = SEQ_CONTROL;
    s->lzma2.need_dict_reset = TRUE;

    s->temp.size = 0;

    return XZ_OK;
}

/*
 * Take care of the LZMA2 control layer, and forward the job of actual LZMA
 * decoding or copying of uncompressed chunks to other functions.
 */
int xz_dec_lzma2_run(t_xz_dec_lzma2 *s, t_xz_buf *b)
{
    uint32_t tmp;

    while (b->in_pos < b->in_size || s->lzma2.sequence == SEQ_LZMA_RUN)
    {
        switch (s->lzma2.sequence)
        {
        case SEQ_CONTROL:
            /*
             * LZMA2 control byte
             *
             * Exact values:
             *   0x00   End marker
             *   0x01   Dictionary reset followed by
             *          an uncompressed chunk
             *   0x02   Uncompressed chunk (no dictionary reset)
             *
             * Highest three bits (s->control & 0xE0):
             *   0xE0   Dictionary reset, new properties and state
             *          reset, followed by LZMA compressed chunk
             *   0xC0   New properties and state reset, followed
             *          by LZMA compressed chunk (no dictionary
             *          reset)
             *   0xA0   State reset using old properties,
             *          followed by LZMA compressed chunk (no
             *          dictionary reset)
             *   0x80   LZMA chunk (no dictionary or state reset)
             *
             * For LZMA compressed chunks, the lowest five bits
             * (s->control & 1F) are the highest bits of the
             * uncompressed size (bits 16-20).
             *
             * A new LZMA2 stream must begin with a dictionary
             * reset. The first LZMA chunk must set new
             * properties and reset the LZMA state.
             *
             * Values that don't match anything described above
             * are invalid and we return XZ_DATA_ERROR.
             */
            tmp = b->in[b->in_pos++];

            if (tmp >= 0xE0 || tmp == 0x01)
            {
                s->lzma2.need_props = TRUE;
                s->lzma2.need_dict_reset = FALSE;
                f_dict_reset(&s->dict, b);
            }
            else if (s->lzma2.need_dict_reset)
            {
                return XZ_ERR_DATA;
            }

            if (tmp >= 0x80)
            {
                s->lzma2.uncompressed = (tmp & 0x1F) << 16;
                s->lzma2.sequence = SEQ_UNCOMPRESSED_1;

                if (tmp >= 0xC0)
                {
                    /*
                     * When there are new properties,
                     * state reset is done at
                     * SEQ_PROPERTIES.
                     */
                    s->lzma2.need_props = FALSE;
                    s->lzma2.next_sequence = SEQ_PROPERTIES;
                }
                else if (s->lzma2.need_props)
                {
                    return XZ_ERR_DATA;
                }
                else
                {
                    s->lzma2.next_sequence = SEQ_LZMA_PREPARE;
                    if (tmp >= 0xA0)
                        f_lzma_reset(s);
                }
            }
            else
            {
                if (tmp == 0x00)
                    return XZ_STREAM_END;

                if (tmp > 0x02)
                    return XZ_ERR_DATA;

                s->lzma2.sequence = SEQ_COMPRESSED_0;
                s->lzma2.next_sequence = SEQ_COPY;
            }
            break;

        case SEQ_UNCOMPRESSED_1:
            s->lzma2.uncompressed += (uint32_t)b->in[b->in_pos++] << 8;
            s->lzma2.sequence = SEQ_UNCOMPRESSED_2;
            break;
        case SEQ_UNCOMPRESSED_2:
            s->lzma2.uncompressed += (uint32_t)b->in[b->in_pos++] + 1;
            s->lzma2.sequence = SEQ_COMPRESSED_0;
            break;
        case SEQ_COMPRESSED_0:
            s->lzma2.compressed = (uint32_t)b->in[b->in_pos++] << 8;
            s->lzma2.sequence = SEQ_COMPRESSED_1;
            break;
        case SEQ_COMPRESSED_1:
            s->lzma2.compressed += (uint32_t)b->in[b->in_pos++] + 1;
            s->lzma2.sequence = s->lzma2.next_sequence;
            break;
        case SEQ_PROPERTIES:
            if (!f_lzma_props(s, b->in[b->in_pos++]))
                return XZ_ERR_DATA;
            s->lzma2.sequence = SEQ_LZMA_PREPARE;
        case SEQ_LZMA_PREPARE:
            if (s->lzma2.compressed < RC_INIT_BYTES)
                return XZ_ERR_DATA;

            if (!f_rc_read_init(&s->rc, b))
                return XZ_OK;

            s->lzma2.compressed -= RC_INIT_BYTES;
            s->lzma2.sequence = SEQ_LZMA_RUN;

        case SEQ_LZMA_RUN:
            /*
             * Set dictionary limit to indicate how much we want
             * to be encoded at maximum. Decode new data into the
             * dictionary. Flush the new data from dictionary to
             * b->out. Check if we finished decoding this chunk.
             * In case the dictionary got full but we didn't fill
             * the output buffer yet, we may run this loop
             * multiple times without changing s->lzma2.sequence.
             */
            f_dict_limit(&s->dict, MIN(b->out_size - b->out_pos,
                    s->lzma2.uncompressed));
            if (!f_lzma2_lzma(s, b))
                return XZ_ERR_DATA;

            s->lzma2.uncompressed -= f_dict_flush(&s->dict, b);

            if (s->lzma2.uncompressed == 0)
            {
                if (s->lzma2.compressed > 0 || s->lzma.len > 0
                        || !f_rc_is_finished(&s->rc))
                    return XZ_ERR_DATA;

                f_rc_reset(&s->rc);
                s->lzma2.sequence = SEQ_CONTROL;
            }
            else if (b->out_pos == b->out_size
                    || (b->in_pos == b->in_size
                        && s->temp.size
                        < s->lzma2.compressed))
            {
                return XZ_OK;
            }

            break;

        case SEQ_COPY:
            f_dict_uncompressed(&s->dict, b, &s->lzma2.compressed);
            if (s->lzma2.compressed > 0)
                return XZ_OK;

            s->lzma2.sequence = SEQ_CONTROL;
            break;
        }
    }

    return XZ_OK;
}


